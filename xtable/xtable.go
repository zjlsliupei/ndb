package xtable

import (
	config "gitee.com/zjlsliupei/ndb/config"
	myData "gitee.com/zjlsliupei/ndb/xtable/data"
	"gitee.com/zjlsliupei/ndb/xtable/form"
)

type XTable struct {
	_config config.Option
}

func NewXTable(_config config.Option) XTable {
	return XTable{_config: _config}
}

func (this *XTable) Form() *form.Form {
	f := form.NewForm(this._config)
	return &f
}

func (this *XTable) Data() *myData.Data {
	d := myData.NewData(this._config)
	return &d
}
