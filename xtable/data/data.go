package form

import "gitee.com/zjlsliupei/ndb/config"

type Data struct {
	option  config.Option
	rpcName string
}

func NewData(option config.Option) Data {
	return Data{
		option:  option,
		rpcName: "rpcx.xtable.data",
	}
}

func (this *Data) List() *List {
	list := NewList(this.option, this.rpcName)
	return &list
}

func (this *Data) Detail() *Detail {
	detail := NewDetail(this.option, this.rpcName)
	return &detail
}
