package form

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type FormDetail struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewFormDetail(option config.Option, rpcName string) FormDetail {
	return FormDetail{
		option:   option,
		rpcName:  rpcName,
		funcName: "FormDetail",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *FormDetail) SetGroupId(groupId string) *FormDetail {
	this.arg["group_id"] = groupId
	return this
}

// SetId form的id,必填
func (this *FormDetail) SetId(id string) *FormDetail {
	this.arg["id"] = id
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *FormDetail) AutoMeta(c *gin.Context) *FormDetail {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *FormDetail) SetMeta(meta ghelp.MAP) *FormDetail {
	this.meta = meta
	return this
}

func (this *FormDetail) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
