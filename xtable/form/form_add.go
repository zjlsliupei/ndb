package form

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type FormAdd struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewFormAdd(option config.Option, rpcName string) FormAdd {
	return FormAdd{
		option:   option,
		rpcName:  rpcName,
		funcName: "FormAdd",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *FormAdd) SetGroupId(groupId string) *FormAdd {
	this.arg["group_id"] = groupId
	return this
}

// SetFormId 需要获取的表单id, 必填
func (this *FormAdd) SetFormId(formId string) *FormAdd {
	this.arg["form_id"] = formId
	return this
}

// SetAppId 所属app的id
func (this *FormAdd) SetAppId(appId string) *FormAdd {
	this.arg["app_id"] = appId
	return this
}

// SetFormClass 表单分类，用于表单分组, 必填
func (this *FormAdd) SetFormClass(formClass string) *FormAdd {
	this.arg["form_class"] = formClass
	return this
}

// SetFormName 表单名称，用于返回列表显示，无特殊含义, 必填
func (this *FormAdd) SetFormName(formName string) *FormAdd {
	this.arg["form_name"] = formName
	return this
}

// SetState 状态，默认0，0正常，1禁用
func (this *FormAdd) SetState(state int64) *FormAdd {
	this.arg["state"] = state
	return this
}

// SetType virtual表示虚拟表
func (this *FormAdd) SetType(strtype string) *FormAdd {
	this.arg["type"] = strtype
	return this
}

// SetVirtual 虚拟表单配置, json
func (this *FormAdd) SetVirtual(virtual string) *FormAdd {
	this.arg["virtual"] = virtual
	return this
}

// SetFormConf 其他配置项，只做存储
func (this *FormAdd) SetFormConf(formConf string) *FormAdd {
	this.arg["form_conf"] = formConf
	return this
}

// SetFormContent 	表单字段内容,详见示例说明, 必填
func (this *FormAdd) SetFormContent(formContent string) *FormAdd {
	this.arg["form_content"] = formContent
	return this
}

// SetFormVer 表单版本号, 必填
func (this *FormAdd) SetFormVer(formVer string) *FormAdd {
	this.arg["form_ver"] = formVer
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *FormAdd) AutoMeta(c *gin.Context) *FormAdd {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *FormAdd) SetMeta(meta ghelp.MAP) *FormAdd {
	this.meta = meta
	return this
}

func (this *FormAdd) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
