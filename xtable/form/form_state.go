package form

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type FormState struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewFormState(option config.Option, rpcName string) FormState {
	return FormState{
		option:   option,
		rpcName:  rpcName,
		funcName: "FormState",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *FormState) SetGroupId(groupId string) *FormState {
	this.arg["group_id"] = groupId
	return this
}

// SetId form的id,必填
func (this *FormState) SetId(id string) *FormState {
	this.arg["id"] = id
	return this
}

// SetState 状态，默认0，0正常，1禁用，必填
func (this *FormState) SetState(state int64) *FormState {
	this.arg["state"] = state
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *FormState) AutoMeta(c *gin.Context) *FormState {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *FormState) SetMeta(meta ghelp.MAP) *FormState {
	this.meta = meta
	return this
}

func (this *FormState) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
