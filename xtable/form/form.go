package form

import "gitee.com/zjlsliupei/ndb/config"

type Form struct {
	option  config.Option
	rpcName string
}

func NewForm(option config.Option) Form {
	return Form{
		option:  option,
		rpcName: "rpcx.xtable.form",
	}
}

func (this *Form) FormAdd() *FormAdd {
	formAdd := NewFormAdd(this.option, this.rpcName)
	return &formAdd
}

func (this *Form) FormList() *FormList {
	formList := NewFormList(this.option, this.rpcName)
	return &formList
}

func (this *Form) FormDetail() *FormDetail {
	formDetail := NewFormDetail(this.option, this.rpcName)
	return &formDetail
}

func (this *Form) FormState() *FormState {
	formState := NewFormState(this.option, this.rpcName)
	return &formState
}
