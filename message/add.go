package message

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Add struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewAdd(option config.Option, rpcName string) Add {
	return Add{
		option:   option,
		rpcName:  rpcName,
		funcName: "Add",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *Add) SetGroupId(groupId string) *Add {
	this.arg["group_id"] = groupId
	return this
}

// SetRows 需要获取的表单id, 必填
func (this *Add) SetRows(rows []interface{}) *Add {
	this.arg["rows"] = rows
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Add) AutoMeta(c *gin.Context) *Add {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Add) SetMeta(meta ghelp.MAP) *Add {
	this.meta = meta
	return this
}

func (this *Add) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
