package message

import "gitee.com/zjlsliupei/ndb/config"

type Message struct {
	option  config.Option
	rpcName string
}

func NewMessage(option config.Option) Message {
	return Message{
		option:  option,
		rpcName: "rpcx.ndb2.message",
	}
}

func (this *Message) Add() *Add {
	add := NewAdd(this.option, this.rpcName)
	return &add
}

func (this *Message) List() *List {
	list := NewList(this.option, this.rpcName)
	return &list
}
