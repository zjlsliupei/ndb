package request

import "github.com/tidwall/gjson"

var (
	BeforeExecCbs []BeforeExecCb
	AfterExecCbs  []AfterExecCb
)

type BeforeExecCb func(arg BeforeExec) error
type AfterExecCb func(arg AfterExec) error

type BeforeExec struct {
	Addr     string
	RpcName  string
	FuncName string
	Args     interface{}
}

type AfterExec struct {
	Addr          string
	RpcName       string
	FuncName      string
	Args          interface{}
	Response      gjson.Result
	ResponseError error
}

// AddBeforeExecFunc 注册前置hook
func AddBeforeExecFunc(cb BeforeExecCb) {
	BeforeExecCbs = append(BeforeExecCbs, cb)
}

// AddAfterExecFunc 注册后置hook
func AddAfterExecFunc(cb AfterExecCb) {
	AfterExecCbs = append(AfterExecCbs, cb)
}
