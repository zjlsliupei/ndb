package table

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Insert struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewInsert(option config.Option, rpcName string) Insert {
	return Insert{
		option:   option,
		rpcName:  rpcName,
		funcName: "Insert",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组,必填
func (this *Insert) SetGroupId(groupId string) *Insert {
	this.arg["group_id"] = groupId
	return this
}

// SetUpdate 如果rows里id发生冲突：0返回错误；1更新；2覆盖(删除重建)
func (this *Insert) SetUpdate(update int64) *Insert {
	this.arg["update"] = update
	return this
}

// SetRows 支持多个数据一起插入, 必填
func (this *Insert) SetRows(rows map[string]interface{}) *Insert {
	this.arg["rows"] = rows
	return this
}

// SetAuthor 操作者，用在历史记录里
func (this *Insert) SetAuthor(author string) *Insert {
	this.arg["author"] = author
	return this
}

// SetModify 1表示返回具体修改的内容，默认不返回
func (this *Insert) SetModify(modify int64) *Insert {
	this.arg["modify"] = modify
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Insert) AutoMeta(c *gin.Context) *Insert {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Insert) SetMeta(meta ghelp.MAP) *Insert {
	this.meta = meta
	return this
}

func (this *Insert) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
