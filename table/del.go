package table

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Del struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewDel(option config.Option, rpcName string) Del {
	return Del{
		option:   option,
		rpcName:  rpcName,
		funcName: "Del",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组, 必填
func (this *Del) SetGroupId(groupId string) *Del {
	this.arg["group_id"] = groupId
	return this
}

// SetFormIdAndId 表单id和id的对应, 必填
func (this *Del) SetFormIdAndId(formIdId map[string]interface{}) *Del {
	this.arg["form_id/id"] = formIdId
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Del) AutoMeta(c *gin.Context) *Del {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Del) SetMeta(meta ghelp.MAP) *Del {
	this.meta = meta
	return this
}

func (this *Del) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
