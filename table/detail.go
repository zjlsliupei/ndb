package table

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Detail struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewDetail(option config.Option, rpcName string) Detail {
	return Detail{
		option:   option,
		rpcName:  rpcName,
		funcName: "Detail",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组, 必填
func (this *Detail) SetGroupId(groupId string) *Detail {
	this.arg["group_id"] = groupId
	return this
}

// SetLock
// 1 表示加锁读取，最多锁定10秒。加锁后，会返回lockids，要调用Unlock来解锁
func (this *Detail) SetLock(lock int64) *Detail {
	this.arg["lock"] = lock
	return this
}

// SetFormIdAndId 表单id和id的对应, 必填
func (this *Detail) SetFormIdAndId(formIdId map[string]interface{}) *Detail {
	this.arg["form_id/id"] = formIdId
	return this
}

// SetFields 指定返回的字段，不填为全部字段
func (this *Detail) SetFields(fields []string) *Detail {
	this.arg["fields"] = fields
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Detail) AutoMeta(c *gin.Context) *Detail {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Detail) SetMeta(meta ghelp.MAP) *Detail {
	this.meta = meta
	return this
}

// Set_Del 设置软删返回数据。可选项，1或2 1:只查询删除的数据，2：查询删除和未删除的数据
func (this *Detail) Set_Del(mode int) *Detail {
	this.arg["_del"] = mode
	return this
}

func (this *Detail) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
