package table

import "gitee.com/zjlsliupei/ndb/config"

type Table struct {
	option  config.Option
	rpcName string
}

func NewTable(option config.Option) Table {
	return Table{
		option:  option,
		rpcName: "rpcx.ndb2.table",
	}
}

func (this *Table) Insert() *Insert {
	insert := NewInsert(this.option, this.rpcName)
	return &insert
}

func (this *Table) Detail() *Detail {
	detail := NewDetail(this.option, this.rpcName)
	return &detail
}

func (this *Table) Unlock() *Unlock {
	unlock := NewUnlock(this.option, this.rpcName)
	return &unlock
}

func (this *Table) Del() *Del {
	del := NewDel(this.option, this.rpcName)
	return &del
}

func (this *Table) Recover() *Recover {
	_recover := NewRecover(this.option, this.rpcName)
	return &_recover
}

func (this *Table) SoftDel() *SoftDel {
	softDel := NewSoftDel(this.option, this.rpcName)
	return &softDel
}

func (this *Table) Pointer() *Pointer {
	pointer := NewPointer(this.option, this.rpcName)
	return &pointer
}
