package table

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type SoftDel struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewSoftDel(option config.Option, rpcName string) SoftDel {
	return SoftDel{
		option:   option,
		rpcName:  rpcName,
		funcName: "SoftDel",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组, 必填
func (this *SoftDel) SetGroupId(groupId string) *SoftDel {
	this.arg["group_id"] = groupId
	return this
}

// SetFormIdAndId 表单id和id的对应, 必填
func (this *SoftDel) SetFormIdAndId(formIdId map[string]interface{}) *SoftDel {
	this.arg["form_id/id"] = formIdId
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *SoftDel) AutoMeta(c *gin.Context) *SoftDel {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *SoftDel) SetMeta(meta ghelp.MAP) *SoftDel {
	this.meta = meta
	return this
}

func (this *SoftDel) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
