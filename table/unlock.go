package table

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Unlock struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewUnlock(option config.Option, rpcName string) Unlock {
	return Unlock{
		option:   option,
		rpcName:  rpcName,
		funcName: "Unlock",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组,必填
func (this *Unlock) SetGroupId(groupId string) *Unlock {
	this.arg["group_id"] = groupId
	return this
}

// SetIds 锁定的ids, 必填
func (this *Unlock) SetIds(ids []string) *Unlock {
	this.arg["ids"] = ids
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Unlock) AutoMeta(c *gin.Context) *Unlock {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Unlock) SetMeta(meta ghelp.MAP) *Unlock {
	this.meta = meta
	return this
}

func (this *Unlock) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
