module gitee.com/zjlsliupei/ndb

go 1.14

require (
	gitee.com/zjlsliupei/ghelp v0.0.15
	github.com/beego/beego/v2 v2.0.1
	github.com/gin-gonic/gin v1.8.1 // indirect
	github.com/tidwall/gjson v1.7.5
)
