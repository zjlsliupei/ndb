package es

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Search struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewSearch(option config.Option, rpcName string) Search {
	return Search{
		option:   option,
		rpcName:  rpcName,
		funcName: "Search",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *Search) SetGroupId(groupId string) *Search {
	this.arg["group_id"] = groupId
	return this
}

// SetFormId 表单id, 必填
func (this *Search) SetFormId(formId string) *Search {
	this.arg["form_id"] = formId
	return this
}

// SetEsquery 搜索条件, 必填
func (this *Search) SetEsquery(esquery map[string]interface{}) *Search {
	this.arg["esquery"] = esquery
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Search) AutoMeta(c *gin.Context) *Search {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Search) SetMeta(meta ghelp.MAP) *Search {
	this.meta = meta
	return this
}

// Set_Del 设置软删返回数据。可选项，1或2 1:只查询删除的数据，2：查询删除和未删除的数据
func (this *Search) Set_Del(mode int) *Search {
	this.arg["_del"] = mode
	return this
}

func (this *Search) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
