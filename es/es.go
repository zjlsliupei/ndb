package es

import "gitee.com/zjlsliupei/ndb/config"

type Es struct {
	option  config.Option
	rpcName string
}

func NewEs(option config.Option) Es {
	return Es{
		option:  option,
		rpcName: "rpcx.ndb2.es",
	}
}

func (this *Es) Search2() *Search2 {
	search2 := NewSearch2(this.option, this.rpcName)
	return &search2
}

func (this *Es) Search() *Search {
	search := NewSearch(this.option, this.rpcName)
	return &search
}

func (this *Es) Join() *Join {
	join := NewJoin(this.option, this.rpcName)
	return &join
}

func (this *Es) Count() *Count {
	count := NewCount(this.option, this.rpcName)
	return &count
}
