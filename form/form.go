package form

import "gitee.com/zjlsliupei/ndb/config"

type Form struct {
	option  config.Option
	rpcName string
}

func NewForm(option config.Option) Form {
	return Form{
		option:  option,
		rpcName: "rpcx.ndb2.form",
	}
}

func (this *Form) FormGet() *FormGet {
	formGet := NewFormGet(this.option, this.rpcName)
	return &formGet
}
