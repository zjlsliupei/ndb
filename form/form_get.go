package form

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type FormGet struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewFormGet(option config.Option, rpcName string) FormGet {
	return FormGet{
		option:   option,
		rpcName:  rpcName,
		funcName: "FormGet",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *FormGet) SetGroupId(groupId string) *FormGet {
	this.arg["group_id"] = groupId
	return this
}

// SetFormId 需要获取的表单id, 必填
func (this *FormGet) SetFormId(formId string) *FormGet {
	this.arg["form_id"] = formId
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *FormGet) AutoMeta(c *gin.Context) *FormGet {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *FormGet) SetMeta(meta ghelp.MAP) *FormGet {
	this.meta = meta
	return this
}

func (this *FormGet) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
