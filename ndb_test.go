package ndb

import (
	"encoding/json"
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"testing"
)

func init() {
	Config(config.Option{
		Addr: "http://116.62.151.136:6544/test",
	})
	//AddBeforeExecHook(func(arg request.BeforeExec) error {
	//	logs.Info("1", arg)
	//	return nil
	//})
	//AddBeforeExecHook(func(arg request.BeforeExec) error {
	//	logs.Info("2", arg)
	//	return nil
	//})
	//
	//AddAfterExecHook(func(arg request.AfterExec) error {
	//	logs.Info("a",arg)
	//	return nil
	//})
}

func TestTable(t *testing.T) {

}

func TestEs(t *testing.T) {
	v := make(map[string]interface{})
	err := json.Unmarshal([]byte(`{
		"from": 0,
		"query": {
		  "bool": {
			"filter": [
			  {
				"match_phrase": {
				  "$product_name": "我"
				}
			  }
			]
		  }
		},
		"size": 10
    }`), &v)
	res, err := Es().Search2().
		SetGroupId("wk347942232105426916").
		SetFormId("c1vts3ab0d0af3btb7j0").
		SetFields([]string{"id", "product_name"}).
		SetMeta(ghelp.MAP{
			"traceid": "11",
			"depth":   1,
		}).
		Set_Del(2).
		SetEsquery(v).
		Do()
	if err != nil {
		t.Error(err)
	}
	t.Log(res.String())
	res, err = Table().SoftDel().
		SetGroupId("wk347942232105426916").
		SetFormIdAndId(map[string]interface{}{"c1vts3ab0d0af3btb7j0": []string{"c20ccdib0d0af3btb820"}}).
		Do()
	t.Log(res.String())
	res, err = Es().Count().
		SetGroupId("wk347942232105426916").
		SetFormId("c1vts3ab0d0af3btb7j0").
		SetMeta(ghelp.MAP{
			"traceid": "11",
			"depth":   1,
		}).
		//Set_Del(1).
		SetEsquery(v).
		Do()
	t.Log(res.String())
}

func TestEsCount(t *testing.T) {
	v := make(map[string]interface{})
	err := json.Unmarshal([]byte(`{
		"query": {
		  "bool": {
			"filter": [
			  {
				"match_phrase": {
				  "$product_name": "我"
				}
			  }
			]
		  }
		}
    }`), &v)
	res, err := Es().Count().
		SetGroupId("wk347942232105426916").
		SetFormId("c1vts3ab0d0af3btb7j0").
		SetMeta(ghelp.MAP{
			"traceid": "11",
			"depth":   1,
		}).
		Set_Del(1).
		SetEsquery(v).
		Do()
	t.Log(res.String(), err)
}
