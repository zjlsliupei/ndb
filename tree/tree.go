package tree

import "gitee.com/zjlsliupei/ndb/config"

type Tree struct {
	option  config.Option
	rpcName string
}

func NewTree(option config.Option) Tree {
	return Tree{
		option:  option,
		rpcName: "rpcx.ndb2.tree",
	}
}

func (this *Tree) GetChildren() *GetChildren {
	getChildren := NewGetChildren(this.option, this.rpcName)
	return &getChildren
}

func (this *Tree) Move() *Move {
	move := NewMove(this.option, this.rpcName)
	return &move
}

func (this *Tree) GetParentPath() *GetParentPath {
	getParentPath := NewGetParentPath(this.option, this.rpcName)
	return &getParentPath
}
