package tree

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type Move struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewMove(option config.Option, rpcName string) Move {
	return Move{
		option:   option,
		rpcName:  rpcName,
		funcName: "Move",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *Move) SetGroupId(groupId string) *Move {
	this.arg["group_id"] = groupId
	return this
}

// SetFormId 所属表单id, 必填
func (this *Move) SetFormId(formId string) *Move {
	this.arg["form_id"] = formId
	return this
}

// SetId 要移动的节点id, 必填
func (this *Move) SetId(id string) *Move {
	this.arg["id"] = id
	return this
}

// SetUpDown up 或 down, 必填
func (this *Move) SetUpDown(updown string) *Move {
	this.arg["updown"] = updown
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *Move) AutoMeta(c *gin.Context) *Move {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *Move) SetMeta(meta ghelp.MAP) *Move {
	this.meta = meta
	return this
}

func (this *Move) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
