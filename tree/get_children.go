package tree

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type GetChildren struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewGetChildren(option config.Option, rpcName string) GetChildren {
	return GetChildren{
		option:   option,
		rpcName:  rpcName,
		funcName: "GetChildren",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *GetChildren) SetGroupId(groupId string) *GetChildren {
	this.arg["group_id"] = groupId
	return this
}

// SetFormIdAndId 表单id和id的对应, 必填
func (this *GetChildren) SetFormIdAndId(formIdId map[string]interface{}) *GetChildren {
	this.arg["form_id/id"] = formIdId
	return this
}

// SetDepth 子节点深度，默认0，表示所有子节点, 不必填
func (this *GetChildren) SetDepth(depth int64) *GetChildren {
	this.arg["depth"] = depth
	return this
}

// SetDetail 是否返回数据详情，1返回。默认1
func (this *GetChildren) SetDetail(detail int64) *GetChildren {
	this.arg["detail"] = detail
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *GetChildren) AutoMeta(c *gin.Context) *GetChildren {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *GetChildren) SetMeta(meta ghelp.MAP) *GetChildren {
	this.meta = meta
	return this
}

func (this *GetChildren) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
