package tree

import (
	"gitee.com/zjlsliupei/ghelp"
	"gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/request"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

type GetParentPath struct {
	option     config.Option
	rpcName    string
	funcName   string
	arg        ghelp.MAP
	meta       ghelp.MAP
	ginContext *gin.Context
}

func NewGetParentPath(option config.Option, rpcName string) GetParentPath {
	return GetParentPath{
		option:   option,
		rpcName:  rpcName,
		funcName: "GetParentPath",
		arg:      make(ghelp.MAP),
	}
}

// SetGroupId 不超过20位，用于数据分组
func (this *GetParentPath) SetGroupId(groupId string) *GetParentPath {
	this.arg["group_id"] = groupId
	return this
}

// SetFormIdAndId 表单id和id的对应, 必填
func (this *GetParentPath) SetFormIdAndId(formIdId map[string]interface{}) *GetParentPath {
	this.arg["form_id/id"] = formIdId
	return this
}

// AutoMeta 自动设置meta，根据gin的上下文来解析，meta; 如果同时使用了Meta优先以Meta为准
func (this *GetParentPath) AutoMeta(c *gin.Context) *GetParentPath {
	_c := c.Copy()
	this.ginContext = _c
	return this
}

// SetMeta 设置meta
func (this *GetParentPath) SetMeta(meta ghelp.MAP) *GetParentPath {
	this.meta = meta
	return this
}

func (this *GetParentPath) Do() (gjson.Result, error) {
	req := request.NewRequest(this.option.Addr)
	req.FuncName(this.funcName)
	req.RpcName(this.rpcName)
	req.AutoMeta(this.ginContext)
	req.SetMeta(this.meta)
	return req.Do(this.arg)
}
