package ndb

import (
	config "gitee.com/zjlsliupei/ndb/config"
	"gitee.com/zjlsliupei/ndb/es"
	"gitee.com/zjlsliupei/ndb/form"
	"gitee.com/zjlsliupei/ndb/message"
	"gitee.com/zjlsliupei/ndb/request"
	"gitee.com/zjlsliupei/ndb/table"
	"gitee.com/zjlsliupei/ndb/tree"
	"gitee.com/zjlsliupei/ndb/xtable"
)

var _config config.Option

func Config(option config.Option) {
	_config = option
}

// Table 实例化table服务
func Table() *table.Table {
	table := table.NewTable(_config)
	return &table
}

// Es 实例化es服务
func Es() *es.Es {
	es := es.NewEs(_config)
	return &es
}

// Form 实例化form服务
func Form() *form.Form {
	form := form.NewForm(_config)
	return &form
}

// Tree 实例化tree服务
func Tree() *tree.Tree {
	tree := tree.NewTree(_config)
	return &tree
}

// AddBeforeExecHook 注册前置hook
func AddBeforeExecHook(cb request.BeforeExecCb) {
	request.AddBeforeExecFunc(cb)
}

// AddAfterExecHook 注册后置hook
func AddAfterExecHook(cb request.AfterExecCb) {
	request.AddAfterExecFunc(cb)
}

// XTable 实例化XTable服务
func XTable() *xtable.XTable {
	xtable := xtable.NewXTable(_config)
	return &xtable
}

// Message 实例化message服务
func Message() *message.Message {
	message := message.NewMessage(_config)
	return &message
}

// Request 实现自定义请求
func Request() request.Request {
	return request.NewRequest(_config.Addr)
}
