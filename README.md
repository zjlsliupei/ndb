# ndb库

## 安装
```shell
go get gitee.com/zjlsliupei/ndb
```

## 快速使用
```go
import (
	"gitee.com/zjlsliupei/ndb"
    "gitee.com/zjlsliupei/ndb/config"
)
// 初始化
ndb.Config(config.Option{
    Addr: "http://127.0.0.1:123",
})

res, err := ndb.Table().
	Insert().
	SetGroupId("1234").
	SetUpdate(1).
	SetRows(rows).	
	Do()

// 增加设置meta有2种方式, 如果2种方式同时设置以SetMeta优先
// 第一种，自动解析gin.Context里面参数到meta中
res, err := ndb.Table().
    Insert().
    SetGroupId("1234").
    SetUpdate(1).
    SetRows(rows).
	AutoMeta(c).
    Do()
// 第二种，手动设置meta
res, err := ndb.Table().
    Insert().
    SetGroupId("1234").
    SetUpdate(1).
    SetRows(rows).
    SetMeta(ghelp.MAP{"traceid":"1111","depth":12}).
    Do()

fmt.Println(res, err)
```

## 注册Hook
可以注册前置hook,后置hook，分别在请求前后执行相应回调；
需要在请求之前注册hook
```go
// 注册前置hook,可以注册多个，执行顺序按注册的顺序
ndb.AddBeforeExecHook(func(arg request.BeforeExec) error {
    logs.Info("before", arg)
    return nil
})

// 注册后置hook,可以注册多个，执行顺序按注册的顺序
ndb.AddAfterExecHook(func(arg request.AfterExec) error {
    logs.Info("after", arg)
    return nil
})
```

## 实现自定义请求
```go
req := Request()
req.RpcName("rpcx.xtable.form")
req.RpcMethod("FormDetail")
// 增加设置meta有2种方式, 如果2种方式同时设置以SetMeta优先
// 第一种，自动解析gin.Context里面参数到meta中
req.AutoMeta(c)
// 第二种，手动设置meta
req.SetMeta(ghelp.MAP{"traceid":"1111","depth":12})

res, err := req.Do(map[string]interface{}{
	"group_id": "xxx",
	"id": "xxx"
})
fmt.Println(res, err)
```